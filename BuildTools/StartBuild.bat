REM Provide the Fallout 4 install directory to import essential files
SET "f4installDir=D:\Games\Steam\steamapps\common\Fallout 4"
REM ==============================================
REM Provide the name of the .esm/.esp/.esl, minus the file extension
REM ==============================================
SET "masterName=DDLL"
REM Provide the extension of the file above: .esm/.esp/.esl
REM ==============================================
SET "masterExtension=.esl"
REM ==============================================
REM Provide the script namespaces
SET "scriptNamespace=DDLL"

@echo off
setlocal EnableDelayedExpansion
set Path_to_convert=%f4installDir%
set Reference_path=c:\
set Result=!Path_to_convert:*%Reference_path%\=!
echo Result: %Result%

SET "mainArchiveName=%masterName% - Main.ba2"
SET "textureArchiveName=%masterName% - Textures.ba2"

REM Run Elric to process source assets
REM ==============================================

REM Create temporary directories to non-destructively modify files
REM ==============================================
mkdir %~dp0\Build

REM Copy Assets to the build workspace
REM ==============================================
xcopy "..\Materials" "%~dp0\Build\Materials\" /E
xcopy "..\LODSettings" "%~dp0\Build\LODSettings\" /E
xcopy "..\Interface" "%~dp0\Build\Interface\" /E
xcopy "..\Sound" "%~dp0\Build\Sound\" /E
xcopy "..\Music" "%~dp0\Build\Music\" /E
xcopy "..\Videos" "%~dp0\Build\Videos\" /E
xcopy "..\Meshes" "%~dp0\Build\Meshes\" /E
xcopy "..\Textures" "%~dp0\Build\Textures\" /E

REM Convert .wav files to .xwm
REM ==============================================
for /f "delims=\" %%a in ("%~dp0\Build\Sound\") do set mod=%%~nxa
FOR /R %%a IN (*.wav) DO %~dp0\Audio\xwmaencode "%%a" "%%~pa%%~na.xwm"

for /f "delims=\" %%a in ("%~dp0\Build\Music\") do set mod=%%~nxa
FOR /R %%a IN (*.wav) DO %~dp0\Audio\xwmaencode "%%a" "%%~pa%%~na.xwm"

REM Build .fuz files in the Sound/Voice folders
REM ==============================================
%~dp0\Audio\LIPFuzer -s="%~dp0\Build\Sound\Voice"

REM Cleanup audio assets after conversion
REM ==============================================
del /S %~dp0\Build\Sound\*.wav
del /S %~dp0\Build\Sound\*.lip
del /S %~dp0\Build\Sound\*.xwm

REM Compile Papyrus PSC files for release, stripping debug code
REM ==============================================
"%Result%\Papyrus Compiler\PapyrusCompiler.exe" %1 "..\Scripts\Source\User" -i="%Result%\Data\Scripts\Source\User";"%Result%\Data\Scripts\Source\Base";"..\Scripts\Source\User" -f="Institute_Papyrus_Flags.flg" -o="Build\Scripts" -all -op -r -final

REM Building the - Main archive
REM ==============================================
%~dp0\Archive2\Archive2.exe "Build\Scripts\%scriptNamespace%","Build\Materials","Build\LODSettings","Build\Interface","Build\Sound","Build\Music","\Build\Videos","Build\Meshes" -root="%cd%\Build" -create="%mainArchiveName%" -format="General" -excludeFile="ArchiveExcludes-Main.txt"

REM Building the - Textures archive
REM ==============================================
%~dp0\Archive2\Archive2.exe "Build\Textures" -root="%cd%\Build" -create="%textureArchiveName%" -format="DDS" -excludeFile="ArchiveExcludes-Textures.txt"

REM Copy the .esm/.esp/.esl file into the same folder as the archives
REM ==============================================
copy "..\%masterName%%masterExtension%" "%cd%"

REM Remove temporary directories
REM ==============================================
rmdir %~dp0\Build /S /Q
pause