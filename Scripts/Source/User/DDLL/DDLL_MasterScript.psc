Scriptname DDLL:DDLL_MasterScript extends Quest
{The master script for DDLL. Checks which supported DLC the player has, and stores relevant Leveled Lists in local vars. Then, adds those items to intermediary
DDLL Leveled Lists, which are themselves injected to the appropriate location. This is so that if the player is determined to uninstall the mod, the items will be removed from said
Leveled Lists, rather than linger (even though they shouldn't ever uninstall a mod mid way through a save game, anyway.) Some parts of the script are based off of VendorListInjectorScript 
provided by BGS - credit goes to them for it.}

; Core functionality.
Group System CollapsedOnRef
SupportedDLCStruct[] Property SupportedDLCs Auto Const
GlobalVariable Property DDLL_Version Auto Const Mandatory
GlobalVariable Property DDLL_DistributeItemsAfterQuestCompleted Auto Const Mandatory
GlobalVariable Property DDLL_OutOfUniverseDistributionAllowed Auto Const Mandatory
Message Property DDLL_Config_Message_InitialSetup Auto Const Mandatory
Message Property DDLL_Config_Message Auto Const Mandatory
Message Property DDLL_Config_Message_Explanation Auto Const Mandatory
Location Property PrewarSanctuaryHillsLocation Auto Const Mandatory
Location Property PrewarVault111Location Auto Const Mandatory
Location Property Vault111Location Auto Const Mandatory ; Added in Version 1.0.2 to fix an issue with Start Me Up.
EndGroup

; Forms to inject into
Group BaseLeveledLists CollapsedOnRef
; Children of Atom
LeveledItem Property LL_GammaGun Auto Const Mandatory
; Gunners
LeveledItem Property LLI_Gunner_Auto Auto Const Mandatory
LeveledItem Property LLI_Gunner_Melee Auto Const Mandatory
LeveledItem Property LLI_Gunner_SemiAuto Auto Const Mandatory
LeveledItem Property LLI_Gunner_SemiAuto_Boss Auto Const Mandatory
LeveledItem Property LLI_Gunner_Sniper Auto Const Mandatory
LeveledItem Property LLI_Gunner_Weapon_High Auto Const Mandatory

LeveledItem Property LLI_Armor_Gunner_AllLevels Auto Const Mandatory
LeveledItem Property LL_Armor_Gunner01 Auto Const Mandatory
LeveledItem Property LLI_Armor_Gunner_Outfit Auto Const Mandatory
; Raiders
LeveledItem Property LLI_Raider_Auto Auto Const Mandatory
LeveledItem Property LLI_Raider_Grenade_frag_15 Auto Const Mandatory
LeveledItem Property LLI_Raider_Melee Auto Const Mandatory
LeveledItem Property LLI_Raider_Melee_Simple Auto Const Mandatory
LeveledItem Property LLI_Raider_Melee_Standard Auto Const Mandatory
LeveledItem Property LLI_Raider_Weapons Auto Const Mandatory
LeveledItem Property LLI_Raider_Weapons_Boss Auto Const Mandatory

LeveledItem Property LLI_Armor_Raider Auto Const Mandatory
LeveledItem Property LLI_Armor_Raider01 Auto Const Mandatory
LeveledItem Property LLI_Armor_Raider02b_3 Auto Const Mandatory
LeveledItem Property LLI_Armor_Raider_Regular_New Auto Const Mandatory
; Super Mutants
LeveledItem Property LLI_Supermutant_Autorifle Auto Const Mandatory
LeveledItem Property LLI_Supermutant_Autorifle_Boss Auto Const Mandatory
LeveledItem Property LLI_Supermutant_BoltAction_Rifle Auto Const Mandatory
LeveledItem Property LLI_Supermutant_Melee Auto Const Mandatory
LeveledItem Property LLI_Supermutant_Minigun Auto Const Mandatory
LeveledItem Property LLI_Supermutant_Minigun_Boss Auto Const Mandatory
LeveledItem Property LLI_Supermutant_Missile Auto Const Mandatory
LeveledItem Property LLI_Supermutant_Semiauto_Rifle Auto Const Mandatory
LeveledItem Property LLI_Supermutant_Semiauto_Rifle_Boss Auto Const Mandatory
; Minutemen
LeveledItem Property LL_Minutemen_Weapons_Auto Auto Const Mandatory
LeveledItem Property LL_Minutemen_Weapons_NonAuto Auto Const Mandatory
LeveledItem Property LL_Minutemen_Weapons_Random Auto Const Mandatory
; BoS
LeveledItem Property LLI_BoSScribe_Weapons Auto Const Mandatory 
LeveledItem Property LLI_BoSSoldier_Auto Auto Const Mandatory
LeveledItem Property LLI_BoSSoldier_MinigunOrGatling Auto Const Mandatory
LeveledItem Property LLI_BoSSoldier_MissileLauncher Auto Const Mandatory
LeveledItem Property LLI_BoSSoldier_SemiAuto Auto Const Mandatory
LeveledItem Property LLI_BoSSoldier_Weapons Auto Const Mandatory

LeveledItem Property LLI_BoSSoldierOutfit Auto Const Mandatory
LeveledItem Property LLI_Armor_BoSUniform Auto Const Mandatory
LeveledItem Property LLI_Armor_BoSCombatArmor_Outfit Auto Const Mandatory
; Railroad
LeveledItem Property LLI_RRAgent_Weapons Auto Const Mandatory
; Triggermen
LeveledItem Property LLI_Triggerman_Weapons_Pistol Auto Const Mandatory
LeveledItem Property LLI_Triggerman_Weapons_Rifle_Auto Auto Const Mandatory
; Institute
LeveledItem Property LLI_Synth_Weapons Auto Const Mandatory
LeveledItem Property LLI_Hostile_Synth_Melee Auto Const Mandatory
LeveledItem Property LLI_Hostile_Synth_Ranged Auto Const Mandatory

; Specific weapon lists
LeveledItem Property LL_Flamer Auto Const Mandatory

; Vendors
LeveledItem Property VL_Vendor_Armor Auto Const Mandatory
LeveledItem Property VL_Vendor_Clothing Auto Const Mandatory
LeveledItem Property VL_Vendor_General Auto Const Mandatory
LeveledItem Property VL_Vendor_General_Drumlin Auto Const Mandatory
LeveledItem Property VL_Vendor_Weapon Auto Const Mandatory
LeveledItem Property LLC_DCShopArmor Auto Const Mandatory
LeveledItem Property LLI_Armor_BoSCombatArmor_Vendor_Outfit Auto Const Mandatory
LeveledItem Property LLC_WorkshopVendorArmor01 Auto Const Mandatory
LeveledItem Property LLC_WorkshopVendorArmor02 Auto Const Mandatory
LeveledItem Property LLC_WorkshopVendorArmor03 Auto Const Mandatory
LeveledItem Property LL_Vendor_Weapon_GunSpecialty Auto Const Mandatory
LeveledItem Property LL_Vendor_Weapon_GunBoS Auto Const Mandatory

; Containers
LeveledItem Property LL_Weapons_Guns_Long Auto Const Mandatory
LeveledItem Property LL_Weapons_Guns_Short Auto Const Mandatory

LeveledItem Property LLC_Weapons_Guns_Raider_Long Auto Const Mandatory
LeveledItem Property LLC_Weapons_Guns_Raider_Short Auto Const Mandatory

LeveledItem Property LL_Ammo_Any Auto Const Mandatory
LeveledItem Property LL_Ammo_Any_Same Auto Const Mandatory

LeveledItem Property LLI_Loot_Ammo_Rare Auto Const Mandatory
LeveledItem Property LLI_Loot_Ammo_Unique Auto Const Mandatory
LeveledItem Property LL_Weapon_Any_Boss Auto Const Mandatory

; Perks
LeveledItem Property LLE_Perk_Scrounger100 Auto Const Mandatory
LeveledItem Property LLE_Perk_ScroungerSmall Auto Const Mandatory
EndGroup

; DDLL's Intermediary Leveled Lists
Group DDLLLeveledLists CollapsedOnRef
; Far Harbor
LeveledItem Property DDLL_LL_Ammo_HarpoonGun Auto Const Mandatory
LeveledItem Property DDLL_LL_HarpoonGun Auto Const Mandatory
LeveledItem Property DDLL_LL_HarpoonGun75 Auto Const Mandatory
LeveledItem Property DDLL_LL_RadiumRifle Auto Const Mandatory
LeveledItem Property DDLL_LL_RadiumRifle_RandomTemplate Auto Const Mandatory
LeveledItem Property DDLL_LL_RadiumRifle_Rifle_Auto Auto Const Mandatory
LeveledItem Property DDLL_LL_RadiumRifle_Rifle_SemiAuto Auto Const Mandatory
LeveledItem Property DDLL_LL_RadiumRifle_ShortRifle_SemiAuto Auto Const Mandatory
LeveledItem Property DDLL_LL_RadiumRifle_Simple Auto Const Mandatory
LeveledItem Property DDLL_LL_RadiumRifle_Sniper Auto Const Mandatory
LeveledItem Property DDLL_LLC_RadiumRifle_75 Auto Const Mandatory
LeveledItem Property DDLL_LLW_Vendor_RadiumRifle Auto Const Mandatory
LeveledItem Property DDLL_LL_MeatHook Auto Const Mandatory
LeveledItem Property DDLL_LL_PoleHook Auto Const Mandatory
LeveledItem Property DDLL_LL_Ammo_LeverGun Auto Const Mandatory
LeveledItem Property DDLL_LL_LeverGun Auto Const Mandatory
LeveledItem Property DDLL_LL_LeverGun75 Auto Const Mandatory
LeveledItem Property DDLL_LL_LeverGun_RandomTemplate Auto Const Mandatory
LeveledItem Property DDLL_LL_LeverGun_Rifle Auto Const Mandatory
LeveledItem Property DDLL_LL_LeverGun_ShortRifle Auto Const Mandatory
LeveledItem Property DDLL_LL_LeverGun_SimpleRifle Auto Const Mandatory
LeveledItem Property DDLL_LL_LeverGun_Sniper Auto Const Mandatory
; Nuka World
LeveledItem Property DDLL_LL_Ammo_HandmadeRound Auto Const Mandatory
LeveledItem Property DDLL_LL_HandMadeGun Auto Const Mandatory
LeveledItem Property DDLL_LL_HandmadeGun_RandomTemplate_Rifle Auto Const Mandatory
LeveledItem Property DDLL_LL_HandmadeGun_Rifle_Auto Auto Const Mandatory
LeveledItem Property DDLL_LL_HandmadeGun_Rifle_SemiAuto Auto Const Mandatory
LeveledItem Property DDLL_LL_HandmadeGun_Rifle_Sniper Auto Const Mandatory
LeveledItem Property DDLL_LL_HandmadeGun_ShortRifle_SemiAuto Auto Const Mandatory
LeveledItem Property DDLL_LL_HandmadeGun_SimpleRifle Auto Const Mandatory
LeveledItem Property DDLL_LL_Revolver Auto Const Mandatory
LeveledItem Property DDLL_LL_Revolver75 Auto Const Mandatory
LeveledItem Property DDLL_LLW_Vendor_HandmadeGun Auto Const Mandatory
; Anti-Materiel Rifle
LeveledItem Property DDLL_LL_AntiMaterielRifle Auto Const Mandatory
LeveledItem Property DDLL_LL_AntiMaterielRifle25 Auto Const Mandatory
; CR-74L
LeveledItem Property DDLL_ccFRSFO4003_LL_CR74L Auto Const Mandatory
LeveledItem Property DDLL_ccFRSFO4003_LL_CR74L75 Auto Const Mandatory
; BFG
LeveledItem Property DDLL_ccBGSF04042_LL_Ammo_BFGPlasmaCell Auto Const Mandatory
LeveledItem Property DDLL_ccBGSF04042_LL_Ammo_BFGPlasmaCell_Vendor Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4042_BFG9000 Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4042_BFG900025 Auto Const Mandatory
; Prototype Gauss Rifle
LeveledItem Property DDLL_ccBGSFO4018_LL_Ammo_GaussRifle Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4018_LL_GaussRifle Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4018_LL_GaussRifle50 Auto Const Mandatory
; Tunnel Snakes Rule
LeveledItem Property DDLL_ccRZRFO4001_LL_Pistol_Classic10mm_Simple Auto Const Mandatory
LeveledItem Property DDLL_ccRZRFO4001_LL_Pistol_Classic10mm_Simple75 Auto Const Mandatory
; Captain Cosmos
LeveledItem Property DDLL_ccSWKFO4001_CC1EnergyGun Auto Const Mandatory 
LeveledItem Property DDLL_ccSWKFO4001_CC1EnergyGun25 Auto Const Mandatory
; Zetan Arsenal
LeveledItem Property DDLL_ccRZRFO4002_LL_Atomizer Auto Const Mandatory
LeveledItem Property DDLL_ccRZRFO4002_LL_Atomizer25 Auto Const Mandatory
LeveledItem Property DDLL_ccRZRFO4002_LL_Disintegrator Auto Const Mandatory
LeveledItem Property DDLL_ccRZRFO4002_LL_Disintegrator25 Auto Const Mandatory
LeveledItem Property DDLL_ccRZRFO4002_AlienShockBaton Auto Const Mandatory
LeveledItem Property DDLL_ccRZRFO4002_AlienShockBaton25 Auto Const Mandatory
; Heavy Incinerator
LeveledItem Property DDLL_ccBGSFO4116_LL_HeavyFlamer Auto Const Mandatory
; Manwell Rifle
LeveledItem Property DDLL_ccSBJFO4002_LL_ManwellRifle Auto Const Mandatory
LeveledItem Property DDLL_ccSBJFO4002_LL_ManwellRifle25 Auto Const Mandatory
LeveledItem Property DDLL_ccSBJFO4002_LL_ManwellRifleNonCarbine Auto Const Mandatory
LeveledItem Property DDLL_ccSBJFO4002_LL_ManwellRifleNonCarbine25 Auto Const Mandatory
; Quake Thunderbolt
LeveledItem Property DDLL_ccBGSFO4047_DefaultThunderbolt Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4047_DefaultThunderbolt25 Auto Const Mandatory
; Tesla Cannon
LeveledItem Property DDLL_ccBGSFO4046_LL_TesCanWeapon Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4046_LL_TesCanWeapon25 Auto Const Mandatory
; Fantasy Hero
LeveledItem Property DDLL_ccBGSFO4048_Sword Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4048_Sword25 Auto Const Mandatory
; Solar Cannon
LeveledItem Property DDLL_ccSBJFO4001_LL_SolarFlare_Rifle_Standard Auto Const Mandatory
LeveledItem Property DDLL_ccSBJFO4001_LL_SolarFlare_Rifle_Standard25 Auto Const Mandatory
; Pint-Sized Slasher
LeveledItem Property DDLL_ccBGSFO4035_PintSizedSlasherKnife Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4035_PintSizedSlasherKnife25 Auto Const Mandatory
; Capital Wasteland Mercenaries
LeveledItem Property DDLL_ccBGSFO4117_CMArmorRandom Auto Const Mandatory
LeveledItem Property DDLL_ccBGSFO4117_CMArmorNoDress Auto Const Mandatory
; Modular Military Backpacks
LeveledItem Property DDLL_FSVFO4001_LVLI_BackpackBase Auto Const Mandatory
LeveledItem Property DDLL_FSVFO4001_LVLI_BackpackBase25 Auto Const Mandatory
; Shroud Manor
LeveledItem Property DDLL_ccZSEFO4002_Deliverer Auto Const Mandatory
LeveledItem Property DDLL_ccZSEFO4002_Deliverer25 Auto Const Mandatory
; Perks
LeveledItem Property DDLL_LLS_Perk_Scrounger_Ammo_HarpoonGun Auto Const Mandatory
LeveledItem Property DDLL_LLS_Perk_Scrounger_Ammo_LeverGun Auto Const Mandatory
LeveledItem Property DDLL_LLS_Perk_Scrounger_Ammo_HandmadeRound Auto Const Mandatory
LeveledItem Property DDLL_LLS_Perk_Scrounger_Ammo_BFGPlasmaCell Auto Const Mandatory
EndGroup

Struct SupportedDLCStruct
	String DLC
	{The plugin/master name of the DLC.}
	GlobalVariable installedVar
	{The Global Variable to use to track its installed state.}
	GlobalVariable distributedVar
	{The Global Variable to use to track if it's been distributed yet.}
	Int inUniverseTier = 0
	{OPTIONAL: Is this DLC item 'in-universe'? 0: Completely, 1: Technically, 2: No}
	Int questToWaitForDec = 0
	{OPTIONAL: Ignore this DLC until the specified quest in DEC has reached or passed a stage.}
	Int questStageToWaitFor = 0
	{OPTIONAL: Ignore this DLC until the specified quest has reached or passed this stage.}
EndStruct

; #### Variables related to maintenance systems ####
Bool HasPerformedFirstLoad = false
Float CurrentStoredVersion = 0.0 ; The version of DDLL this script is currently aware of.
Int ErrorCode = 0 ; Used for printing appropriate messages to the user's log. 0 means no error.

; #### Far Harbor Forms ####
LeveledItem DLC03_LL_Ammo_HarpoonGun ; 01010B82
Ammo DLC03_AmmoHarpoon ; 01010B80
LeveledItem DLC03_LL_HarpoonGun ; 01020C4C
LeveledItem DLC03_LL_HarpoonGun75 ; 0104FF74
LeveledItem DLC03_LL_RadiumRifle ; 01040808
LeveledItem DLC03_LL_RadiumRifle_RandomTemplate ; 01040809
LeveledItem DLC03_LL_RadiumRifle_Rifle_Auto ; 0104080A
LeveledItem DLC03_LL_RadiumRifle_Rifle_SemiAuto ; 0104080B
LeveledItem DLC03_LL_RadiumRifle_ShortRifle_SemiAuto ; 0104080C
LeveledItem DLC03_LL_RadiumRifle_Simple ; 0104080D
LeveledItem DLC03_LL_RadiumRifle_Sniper ; 0104080E
LeveledItem DLC03_LLC_RadiumRifle_75 ; 0104080F
LeveledItem DLC03_LLW_Vendor_RadiumRifle ; 01040810
LeveledItem DLC03_LL_MeatHook ; 0104FF72
LeveledItem DLC03_LL_PoleHook ; 01034E7A
LeveledItem DLC03_LL_Ammo_LeverGun ; 01042E72
Ammo DLC03_Ammo4570 ; 0102C8B1
LeveledItem DLC03_LL_LeverGun ; 01042E71
LeveledItem DLC03_LL_LeverGun75 ; 0104FF70
LeveledItem DLC03_LL_LeverGun_RandomTemplate ; 0104A651
LeveledItem DLC03_LL_LeverGun_Rifle ; 0104A652
LeveledItem DLC03_LL_LeverGun_ShortRifle ; 0104A653
LeveledItem DLC03_LL_LeverGun_SimpleRifle ; 0104A654
LeveledItem DLC03_LL_LeverGun_Sniper ; 0104A655
; #### Nuka World Forms ####
LeveledItem DLC04_LL_Ammo_HandmadeRound ; 01037895
Ammo DLC04_Ammo_HandmadeRound ; 01037897
LeveledItem DLC04_LL_HandMadeGun ; 01036C23
LeveledItem DLC04_LL_HandmadeGun_RandomTemplate_Rifle ; 01037894
LeveledItem DLC04_LL_HandmadeGun_Rifle_Auto ; 01037893
LeveledItem DLC04_LL_HandmadeGun_Rifle_SemiAuto ; 01037892
LeveledItem DLC04_LL_HandmadeGun_Rifle_Sniper ; 01037891
LeveledItem DLC04_LL_HandmadeGun_ShortRifle_SemiAuto ; 01037890
LeveledItem DLC04_LL_HandmadeGun_SimpleRifle ; 0103788F
LeveledItem DLC04_LL_Revolver ; 0104F4DE
LeveledItem DLC04_LLW_Vendor_HandmadeGun ; 0103788E
; #### Anti-Material Rifle Forms ####
Weapon ccFRSFO4002_AntimaterielRifle ; 01000F9C
; #### CR-74L Rifle Forms ####
LeveledItem ccFRSFO4003_LL_CR74L ; 010008AC
; #### BFG Forms ####
LeveledItem ccBGSF04042_LL_Ammo_BFGPlasmaCell ; 01000819
LeveledItem ccBGSF04042_LL_Ammo_BFGPlasmaCell_Vendor ; 0100081A
Ammo ccBGSFO4042_BFGAmmoPlasmaCell ; 01000804
Weapon ccBGSFO4042_BFG9000 ; 01000800
; #### Prototype Gauss Rifle Forms ####
LeveledItem ccBGSFO4018_LL_Ammo_GaussRifle ; 01000855
LeveledItem ccBGSFO4018_LL_GaussRifle ; 01000801
; #### Tunnel Snakes Forms ####
LeveledItem ccRZRFO4001_LL_Pistol_Classic10mm_Simple ; 01000E9A
; #### Captain Cosmos Forms ####
Weapon ccSWKFO4001_CC1EnergyGun ; 01000864
; #### Zetan Arsenal Forms ####
LeveledItem ccRZRFO4002_LL_Atomizer ; 01000881
LeveledItem ccRZRFO4002_LL_Disintegrator ; 010008AD
Weapon ccRZRFO4002_AlienShockBaton ; 0100084F
; #### Heavy Incinerator Forms ####
LeveledItem ccBGSFO4116_LL_HeavyFlamer ; 0100082B
; #### Manwell Rifle Forms ####
LeveledItem ccSBJFO4002_LL_ManwellRifle ; 0100091D
LeveledItem ccSBJFO4002_LL_ManwellRifleNonCarbine ; 0100099A
; #### Quake Thunderbolt Forms ####
LeveledItem ccBGSFO4047_DefaultThunderbolt ; 01000872
; #### Tesla Cannon Forms ####
LeveledItem ccBGSFO4046_LL_TesCanWeapon ; 0100001E
; #### Fantasy Hero Set Forms ####
Weapon ccBGSFO4048_Sword ; 01000F9B
; #### Solar Cannon Forms ####
LeveledItem ccSBJFO4001_LL_SolarFlare_Rifle_Standard ; 010008D5
; #### Pint-Sized Slasher Forms ####
Weapon ccBGSFO4035_PintSizedSlasherKnife ; 01000809
; #### Capital Wasteland Mercenary Forms ####
LeveledItem ccBGSFO4117_CMArmorRandom ; 01000039
LeveledItem ccBGSFO4117_CMArmorNoDress ; 01000770
; #### Modular Military Backpacks ####
LeveledItem FSVFO4001_LVLI_BackpackBase ; 01000919
; #### Shroud Manor ####
Weapon ccZSEFO4002_Deliverer ; 01001202

Event OnInit()
	If HasPerformedFirstLoad == false ; Has an OnLoad on this script ever run before? If not, force the RunMaintenance() function once.
		utility.wait(2.0) ; Wait a few seconds to allow the game to load in correctly, so that the Configuration Message can't cause issues.
		if Game.GetPlayer().IsInLocation(PrewarSanctuaryHillsLocation) ; The player is in chargen, so delay startup until they've left.
			RegisterForRemoteEvent(Game.GetPlayer(), "OnLocationChange")
		else
			DDLLConfigMessage()
			RunMaintenance()
			HasPerformedFirstLoad = true
		endif
	endif
EndEvent

Event Actor.OnLocationChange(Actor akSender, Location akOldLoc, Location akNewLoc)
	if akNewLoc != PrewarSanctuaryHillsLocation && akNewLoc != PrewarVault111Location && akNewLoc != Vault111Location
		utility.wait(2.0) ; Wait a few seconds to allow the game to load in correctly, so that the Configuration Message can't cause issues.
		DDLLConfigMessage()
		RunMaintenance()
		HasPerformedFirstLoad = true
		UnregisterForRemoteEvent(Game.GetPlayer(), "OnLocationChange")
	else
		RegisterForRemoteEvent(Game.GetPlayer(), "OnLocationChange")
	endif
EndEvent

Event Actor.OnPlayerLoadGame(Actor akSender)
	RunMaintenance()
EndEvent

Function DDLLConfigMessage(Int iButton = 0, Bool showConfig = true)
	InputEnableLayer configMessageLayer = InputEnableLayer.Create()
	configMessageLayer.DisablePlayerControls(true, true, true, true, true, true, true, true, true, true, true)
	if HasPerformedFirstLoad == false
		while showConfig
			if iButton != -1
				iButton = DDLL_Config_Message_InitialSetup.show()
				if iButton == 0 ; Explanation
					DDLL_Config_Message_Explanation.show()
				elseif iButton == 1 ; Only Distribute CreationClub items after their quest has been completed [ON]
					DDLL_DistributeItemsAfterQuestCompleted.SetValue(0)
				elseif iButton == 2 ; Only Distribute CreationClub items after their quest has been completed [OFF]
					DDLL_DistributeItemsAfterQuestCompleted.SetValue(1)
				elseif iButton == 3 ; Allowed Item tier: Standard
					DDLL_OutOfUniverseDistributionAllowed.SetValue(1)
				elseif iButton == 4 ; Allowed Item tier: Wacky
					DDLL_OutOfUniverseDistributionAllowed.SetValue(2)
				elseif iButton == 5 ; Allowed Item tier: Unhinged
					DDLL_OutOfUniverseDistributionAllowed.SetValue(0)
				elseif iButton == 6 ; Done
					showConfig = false
				endif
			endif
		EndWhile
	elseif HasPerformedFirstLoad == true
		while showConfig
			if iButton != -1
				iButton = DDLL_Config_Message.show()
				if iButton == 0 ; Explanation
					DDLL_Config_Message_Explanation.show()
				elseif iButton == 1 ; Only Distribute CreationClub items after their quest has been completed [ON]
					DDLL_DistributeItemsAfterQuestCompleted.SetValue(0)
				elseif iButton == 2 ; Only Distribute CreationClub items after their quest has been completed [OFF]
					DDLL_DistributeItemsAfterQuestCompleted.SetValue(1)
				elseif iButton == 3 ; Allowed Item tier: Standard
					DDLL_OutOfUniverseDistributionAllowed.SetValue(1)
				elseif iButton == 4 ; Allowed Item tier: Wacky
					DDLL_OutOfUniverseDistributionAllowed.SetValue(2)
				elseif iButton == 5 ; Allowed Item tier: Unhinged
					DDLL_OutOfUniverseDistributionAllowed.SetValue(0)
				elseif iButton == 6 ; Re-run distribution code
					RunMaintenance()
				elseif iButton == 7 ; Done
					showConfig = false
				endif
			endif
		EndWhile
	endif
	configMessageLayer.delete()
EndFunction

Function RunMaintenance()
	if DDLL_Version.GetValue() != CurrentStoredVersion && HasPerformedFirstLoad == true; A new version has loaded, run all maintenance scripts.
		debug.trace( self + "DDLL has detected an upgrade from version " + CurrentStoredVersion + " to " + DDLL_Version.GetValue() + ", running relevant upgrade functions now.")

		; ================= VERSION 1.0.2 =================
		; Before 1.0.2, ammo wasn't added to the Scrounger Perk. As of 1.0.2, the only relevant ammo to add
		; Is the Far Harbor Harpoon Gun, Far Harbor Lever Action Rifle, Nuka World Handmade, and Doom BFG ammo.
		; The following is done here.
		if CurrentStoredVersion < 1.02
			debug.trace( self + "Upgrading to 1.0.2, seeding ammo to Scrounger Perk leveled lists.")
			if SupportedDLCs[0].installedVar.GetValue() == 1 && SupportedDLCs[0].distributedVar.GetValue() == 1 ; Far Harbor has already been installed and distributed, so add ammo to Scrounger Perk.
				DLC03_AmmoHarpoon = Game.GetFormFromFile(0x010B80, SupportedDLCs[0].DLC) as Ammo
				DLC03_Ammo4570 = Game.GetFormFromFile(0x02C8B1, SupportedDLCs[0].DLC) as Ammo

				DDLL_LLS_Perk_Scrounger_Ammo_HarpoonGun.AddForm(DLC03_AmmoHarpoon, 1, 1)
				DDLL_LLS_Perk_Scrounger_Ammo_LeverGun.AddForm(DLC03_Ammo4570, 1, 1)

				AddToScroungerPerk(DDLL_LLS_Perk_Scrounger_Ammo_HarpoonGun, 5, 2, true)
				AddToScroungerPerk(DDLL_LLS_Perk_Scrounger_Ammo_LeverGun, 14, 8)
			endif
			if SupportedDLCs[1].installedVar.GetValue() == 1 && SupportedDLCs[0].distributedVar.GetValue() == 1 ; Nuka World has already been installed and distributed, so add ammo to Scrounger Perk.
				DLC04_Ammo_HandmadeRound = Game.GetFormFromFile(0x037897, SupportedDLCs[1].DLC) as Ammo
				DDLL_LLS_Perk_Scrounger_Ammo_HandmadeRound.AddForm(DLC04_Ammo_HandmadeRound, 1, 1)
				AddToScroungerPerk(DDLL_LLS_Perk_Scrounger_Ammo_HandmadeRound, 26, 20)
			endif
			if SupportedDLCs[4].installedVar.GetValue() == 1 && SupportedDLCs[0].distributedVar.GetValue() == 1 ; Doom BFG has already been installed and distributed, so add ammo to Scrounger Perk.
				ccBGSFO4042_BFGAmmoPlasmaCell = Game.GetFormFromFile(0x000804, SupportedDLCs[4].DLC) as Ammo
				DDLL_LLS_Perk_Scrounger_Ammo_BFGPlasmaCell.AddForm(ccBGSFO4042_BFGAmmoPlasmaCell, 1, 1)
				AddToScroungerPerk(DDLL_LLS_Perk_Scrounger_Ammo_BFGPlasmaCell, 18, 10)			
			endif
		endif

		CurrentStoredVersion = DDLL_Version.GetValue()
	elseif HasPerformedFirstLoad == false ; This is the first load, so store the current version.
    	debug.trace( self + "This is DDLL's first setup, store the current version.")
    	CurrentStoredVersion = DDLL_Version.GetValue()
    elseif DDLL_Version.GetValue() == CurrentStoredVersion
    	debug.trace( self + "No DDLL upgrade detected. Installed version is: " + CurrentStoredVersion + ", pausing maintenance script.")
    endif

    ; Check for installed DLCs.
    int currentDLC = 0
    While (currentDLC < SupportedDLCs.length)
    	SupportedDLCStruct CurrentStruct = SupportedDLCs[currentDLC]
    	CheckForDLCInstalled(CurrentStruct.DLC, CurrentStruct.installedVar, CurrentStruct.distributedVar, CurrentStruct.inUniverseTier, CurrentStruct.questToWaitForDec, CurrentStruct.questStageToWaitFor)
    	GetDLCLeveledLists(CurrentStruct.DLC, CurrentStruct.installedVar, CurrentStruct.distributedVar, CurrentStruct.inUniverseTier)
    	currentDLC += 1
    	ErrorCode = 0
    EndWhile

	; Reset the Error Code, as a failsafe.
	ErrorCode = 0
	RegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
EndFunction

Function CheckForDLCInstalled(String DLC, GlobalVariable installedVar, GlobalVariable distributedVar, Int inUniverseTier, Int questToWaitForDec, Int questStageToWaitFor)
	if Game.IsPluginInstalled(DLC)
		; Check to see if this DLC is within the Distribution Tier allowed by the player.
		if inUniverseTier <= DDLL_OutOfUniverseDistributionAllowed.GetValue()
			Quest questToCheck = none
			; Check if there's a quest we should be aware of.
			if questToWaitForDec != 0
				questToCheck = Game.GetFormFromFile(questToWaitForDec, DLC) as Quest
			endif

			; If there's no quest to care about, or the player has set the setting not to care about it, go ahead and enable distribution.
			if questToCheck == none || DDLL_DistributeItemsAfterQuestCompleted.GetValue() == 0
				installedVar.setValue(1) ; This DLC is installed.
				debug.trace( self + "DDLL found " + DLC + ", user tier: " + DDLL_OutOfUniverseDistributionAllowed.GetValue() + ", item tier: " + inUniverseTier + ", flagging for potential distribution.")
			; If there is a quest to care about, do the check here.
			elseif DDLL_DistributeItemsAfterQuestCompleted.GetValue() == 1 &&  questStageToWaitFor <= questToCheck.GetStage()
				installedVar.setValue(1) ; This DLC is installed.
				debug.trace( self + "DDLL found " + DLC + ", user tier: " + DDLL_OutOfUniverseDistributionAllowed.GetValue() + ", item tier: " + inUniverseTier + ", and quest requirements have been met. Flagging for potential distribution.")
			elseif DDLL_DistributeItemsAfterQuestCompleted.GetValue() == 1 && questStageToWaitFor > questToCheck.GetStage()
				installedVar.setValue(0) ; This DLC is not installed.
				distributedVar.setValue(0) ; This DLC is not distributed.
				ErrorCode = 1
				debug.trace( self + "DDLL found " + DLC + ", user tier: " + DDLL_OutOfUniverseDistributionAllowed.GetValue() + ", item tier: " + inUniverseTier + ", but the quest requirements have not been met. Disabling distribution.")
			endif
			
		else
			installedVar.setValue(0) ; This DLC is not installed.
			distributedVar.setValue(0) ; This DLC is not distributed.
			ErrorCode = 2
			debug.trace( self + "DDLL found " + DLC + ", but its item tier of: " + inUniverseTier + " is above the accepted user tier of " + DDLL_OutOfUniverseDistributionAllowed.GetValue() + ". Disabling distribution.")
		endif
	else
		installedVar.setValue(0) ; This DLC is not installed.
		distributedVar.setValue(0) ; This DLC is not distributed.
		ErrorCode = 3
		debug.trace( self + "DDLL did not find " + DLC + ", disabling distribution.")
	endif
EndFunction

Function GetDLCLeveledLists(String DLC, GlobalVariable installedVar, GlobalVariable distributedVar, Int inUniverseTier = 0)
	; Stores the LeveledLists of all installed DLC.
	if DLC == "DLCCoast.esm" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		DLC03_LL_Ammo_HarpoonGun = Game.GetFormFromFile(0x010B82, DLC) as LeveledItem
		DLC03_AmmoHarpoon = Game.GetFormFromFile(0x010B80, DLC) as Ammo
		DLC03_LL_HarpoonGun = Game.GetFormFromFile(0x020C4C, DLC) as LeveledItem
		DLC03_LL_HarpoonGun75 = Game.GetFormFromFile(0x04FF74, DLC) as LeveledItem
		DLC03_LL_RadiumRifle = Game.GetFormFromFile(0x040808, DLC) as LeveledItem
		DLC03_LL_RadiumRifle_RandomTemplate = Game.GetFormFromFile(0x040809, DLC) as LeveledItem
		DLC03_LL_RadiumRifle_Rifle_Auto = Game.GetFormFromFile(0x04080A, DLC) as LeveledItem
		DLC03_LL_RadiumRifle_Rifle_SemiAuto = Game.GetFormFromFile(0x04080B, DLC) as LeveledItem
		DLC03_LL_RadiumRifle_ShortRifle_SemiAuto = Game.GetFormFromFile(0x04080C, DLC) as LeveledItem
		DLC03_LL_RadiumRifle_Simple = Game.GetFormFromFile(0x04080D, DLC) as LeveledItem
		DLC03_LL_RadiumRifle_Sniper = Game.GetFormFromFile(0x04080E, DLC) as LeveledItem
		DLC03_LLC_RadiumRifle_75 = Game.GetFormFromFile(0x04080F, DLC) as LeveledItem
		DLC03_LLW_Vendor_RadiumRifle = Game.GetFormFromFile(0x040810, DLC) as LeveledItem
		DLC03_LL_MeatHook = Game.GetFormFromFile(0x04FF72, DLC) as LeveledItem
		DLC03_LL_PoleHook = Game.GetFormFromFile(0x034E7A, DLC) as LeveledItem
		DLC03_LL_Ammo_LeverGun = Game.GetFormFromFile(0x042E72, DLC) as LeveledItem
		DLC03_Ammo4570 = Game.GetFormFromFile(0x02C8B1, DLC) as Ammo
		DLC03_LL_LeverGun = Game.GetFormFromFile(0x042E71, DLC) as LeveledItem
		DLC03_LL_LeverGun75 = Game.GetFormFromFile(0x04FF70, DLC) as LeveledItem
		DLC03_LL_LeverGun_RandomTemplate = Game.GetFormFromFile(0x04A651, DLC) as LeveledItem
		DLC03_LL_LeverGun_Rifle = Game.GetFormFromFile(0x04A652, DLC) as LeveledItem
		DLC03_LL_LeverGun_ShortRifle = Game.GetFormFromFile(0x04A653, DLC) as LeveledItem
		DLC03_LL_LeverGun_SimpleRifle = Game.GetFormFromFile(0x04A654, DLC) as LeveledItem
		DLC03_LL_LeverGun_Sniper = Game.GetFormFromFile(0x04A655, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_LL_Ammo_HarpoonGun.AddForm(DLC03_LL_Ammo_HarpoonGun, 1, 1)
		DDLL_LLS_Perk_Scrounger_Ammo_HarpoonGun.AddForm(DLC03_AmmoHarpoon, 1, 1)
		DDLL_LL_HarpoonGun.AddForm(DLC03_LL_HarpoonGun, 1, 1)
		DDLL_LL_HarpoonGun75.AddForm(DLC03_LL_HarpoonGun75, 1, 1)
		DDLL_LL_RadiumRifle.AddForm(DLC03_LL_RadiumRifle, 1, 1)
		DDLL_LL_RadiumRifle_RandomTemplate.AddForm(DLC03_LL_RadiumRifle_RandomTemplate, 1, 1)
		DDLL_LL_RadiumRifle_Rifle_Auto.AddForm(DLC03_LL_RadiumRifle_Rifle_Auto, 1, 1)
		DDLL_LL_RadiumRifle_Rifle_SemiAuto.AddForm(DLC03_LL_RadiumRifle_Rifle_SemiAuto, 1, 1)
		DDLL_LL_RadiumRifle_ShortRifle_SemiAuto.AddForm(DLC03_LL_RadiumRifle_ShortRifle_SemiAuto, 1, 1)
		DDLL_LL_RadiumRifle_Simple.AddForm(DLC03_LL_RadiumRifle_Simple, 1, 1)
		DDLL_LL_RadiumRifle_Sniper.AddForm(DLC03_LL_RadiumRifle_Sniper, 1, 1)
		DDLL_LLC_RadiumRifle_75.AddForm(DLC03_LLC_RadiumRifle_75, 1, 1)
		DDLL_LLW_Vendor_RadiumRifle.AddForm(DLC03_LLW_Vendor_RadiumRifle, 1, 1)
		DDLL_LL_MeatHook.AddForm(DLC03_LL_MeatHook, 1, 1)
		DDLL_LL_PoleHook.AddForm(DLC03_LL_PoleHook, 1, 1)
		DDLL_LL_Ammo_LeverGun.AddForm(DLC03_LL_Ammo_LeverGun, 1, 1)
		DDLL_LLS_Perk_Scrounger_Ammo_LeverGun.AddForm(DLC03_Ammo4570, 1, 1)
		DDLL_LL_LeverGun.AddForm(DLC03_LL_LeverGun, 1, 1)
		DDLL_LL_LeverGun75.AddForm(DLC03_LL_LeverGun75, 1, 1)
		DDLL_LL_LeverGun_RandomTemplate.AddForm(DLC03_LL_LeverGun_RandomTemplate, 1, 1)
		DDLL_LL_LeverGun_Rifle.AddForm(DLC03_LL_LeverGun_Rifle, 1, 1)
		DDLL_LL_LeverGun_ShortRifle.AddForm(DLC03_LL_LeverGun_ShortRifle, 1, 1)
		DDLL_LL_LeverGun_SimpleRifle.AddForm(DLC03_LL_LeverGun_SimpleRifle, 1, 1)
		DDLL_LL_LeverGun_Sniper.AddForm(DLC03_LL_LeverGun_Sniper, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Children of Atom
		LL_GammaGun.AddForm(DDLL_LL_RadiumRifle_Simple, 12, 1)
		; Raiders
		LLI_Raider_Melee_Simple.AddForm(DDLL_LL_MeatHook, 1, 1)
		LLI_Raider_Melee_Standard.AddForm(DDLL_LL_PoleHook, 1, 1)
		LLI_Raider_Weapons.AddForm(DDLL_LL_LeverGun_SimpleRifle, 17, 1)
		LLI_Raider_Weapons_Boss.AddForm(DDLL_LL_LeverGun_RandomTemplate, 28, 1)
		; Gunners
		LLI_Gunner_SemiAuto.AddForm(DDLL_LL_LeverGun_Rifle, 20, 1)
		LLI_Gunner_Sniper.AddForm(DDLL_LL_LeverGun_Sniper, 25, 1)
		; Super Mutants
		LLI_Supermutant_Melee.AddForm(DDLL_LL_PoleHook, 1, 1)
		; Railroad
		LLI_RRAgent_Weapons.AddForm(DDLL_LL_LeverGun_SimpleRifle, 7, 1)
		; Minutemen
		LL_Minutemen_Weapons_NonAuto.AddForm(DDLL_LL_LeverGun_SimpleRifle, 7, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_LLC_RadiumRifle_75, 1, 2, false)
		AddToVendorWeaponLists(DDLL_LL_Ammo_HarpoonGun, 8, 5, false)
		AddToVendorWeaponLists(DDLL_LL_HarpoonGun75, 8, 1, false)
		AddToVendorWeaponLists(DDLL_LL_Ammo_LeverGun, 17, 10)
		AddToVendorWeaponLists(DDLL_LL_LeverGun75, 17, 2)
		; Containers
		AddToAmmoLists(DDLL_LL_Ammo_HarpoonGun, 8, 1, "Unique")
		AddToAmmoLists(DDLL_LL_Ammo_LeverGun, 19, 1)
		AddToLongWeaponLists(DDLL_LL_LeverGun_SimpleRifle, 19, 1, true, true)
		AddToLongWeaponLists(DDLL_LL_RadiumRifle, 19, 1, false, true)
		; Scrounger Perk
		AddToScroungerPerk(DDLL_LLS_Perk_Scrounger_Ammo_HarpoonGun, 5, 2, true)
		AddToScroungerPerk(DDLL_LLS_Perk_Scrounger_Ammo_LeverGun, 14, 8)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")	
	elseif DLC == "DLCNukaWorld.esm" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		DLC04_LL_Ammo_HandmadeRound = Game.GetFormFromFile(0x037895, DLC) as LeveledItem
		DLC04_Ammo_HandmadeRound = Game.GetFormFromFile(0x037897, DLC) as Ammo
		DLC04_LL_HandMadeGun = Game.GetFormFromFile(0x036C23, DLC) as LeveledItem
		DLC04_LL_HandmadeGun_RandomTemplate_Rifle = Game.GetFormFromFile(0x037894, DLC) as LeveledItem
		DLC04_LL_HandmadeGun_Rifle_Auto = Game.GetFormFromFile(0x037893, DLC) as LeveledItem
		DLC04_LL_HandmadeGun_Rifle_SemiAuto = Game.GetFormFromFile(0x037892, DLC) as LeveledItem
		DLC04_LL_HandmadeGun_Rifle_Sniper = Game.GetFormFromFile(0x037891, DLC) as LeveledItem
		DLC04_LL_HandmadeGun_ShortRifle_SemiAuto = Game.GetFormFromFile(0x037890, DLC) as LeveledItem
		DLC04_LL_HandmadeGun_SimpleRifle = Game.GetFormFromFile(0x03788F, DLC) as LeveledItem
		DLC04_LL_Revolver = Game.GetFormFromFile(0x04F4DE, DLC) as LeveledItem
		DLC04_LLW_Vendor_HandmadeGun = Game.GetFormFromFile(0x03788E, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_LL_Ammo_HandmadeRound.AddForm(DLC04_LL_Ammo_HandmadeRound, 1, 1)
		DDLL_LLS_Perk_Scrounger_Ammo_HandmadeRound.AddForm(DLC04_Ammo_HandmadeRound, 1, 1)
		DDLL_LL_HandMadeGun.AddForm(DLC04_LL_HandMadeGun, 1, 1)
		DDLL_LL_HandmadeGun_RandomTemplate_Rifle.AddForm(DLC04_LL_HandmadeGun_RandomTemplate_Rifle, 1, 1)
		DDLL_LL_HandmadeGun_Rifle_Auto.AddForm(DLC04_LL_HandmadeGun_Rifle_Auto, 1, 1)
		DDLL_LL_HandmadeGun_Rifle_SemiAuto.AddForm(DLC04_LL_HandmadeGun_Rifle_SemiAuto, 1, 1)
		DDLL_LL_HandmadeGun_Rifle_Sniper.AddForm(DLC04_LL_HandmadeGun_Rifle_Sniper, 1, 1)
		DDLL_LL_HandmadeGun_ShortRifle_SemiAuto.AddForm(DLC04_LL_HandmadeGun_ShortRifle_SemiAuto, 1, 1)
		DDLL_LL_HandmadeGun_SimpleRifle.AddForm(DLC04_LL_HandmadeGun_SimpleRifle, 1, 1)
		DDLL_LL_Revolver.AddForm(DLC04_LL_Revolver, 1, 1)
		DDLL_LLW_Vendor_HandmadeGun.AddForm(DLC04_LLW_Vendor_HandmadeGun, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Raiders
		LLI_Raider_Weapons.AddForm(DDLL_LL_Revolver, 22, 1)
		LLI_Raider_Weapons.AddForm(DDLL_LL_HandmadeGun_SimpleRifle, 28, 1)
		LLI_Raider_Weapons_Boss.AddForm(DDLL_LL_HandmadeGun_Rifle_SemiAuto, 14, 1)
		; Gunners
		LLI_Gunner_Auto.AddForm(DDLL_LL_HandmadeGun_Rifle_Auto, 34, 1)
		LLI_Gunner_SemiAuto.AddForm(DDLL_LL_HandmadeGun_ShortRifle_SemiAuto, 28, 1)
		LLI_Gunner_SemiAuto_Boss.AddForm(DDLL_LL_HandmadeGun_Rifle_SemiAuto, 35, 1)
		LLI_Gunner_Sniper.AddForm(DDLL_LL_HandmadeGun_Rifle_Sniper, 36, 1)
		LLI_Gunner_Weapon_High.AddForm(DDLL_LL_HandMadeGun, 28, 1)
		; Railroad
		LLI_RRAgent_Weapons.AddForm(DDLL_LL_HandmadeGun_Rifle_SemiAuto, 28, 1)
		; Minutemen
		LL_Minutemen_Weapons_Auto.AddForm(DDLL_LL_HandmadeGun_Rifle_Auto, 28, 1)
		LL_Minutemen_Weapons_NonAuto.AddForm(DDLL_LL_HandmadeGun_SimpleRifle, 20, 1)
		LL_Minutemen_Weapons_NonAuto.AddForm(DDLL_LL_Revolver, 20, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_LLW_Vendor_HandmadeGun, 28, 1)
		AddToVendorWeaponLists(DDLL_LL_Ammo_HandmadeRound, 28, 1)
		AddToVendorWeaponLists(DDLL_LL_Revolver75, 22, 1)
		; Containers
		AddToAmmoLists(DDLL_LL_Ammo_HandmadeRound, 28, 1)
		AddToLongWeaponLists(DDLL_LL_HandmadeGun_Rifle_SemiAuto, 28, 1, true, true)
		AddToShortWeaponLists(DDLL_LL_Revolver, 22, 1, true, true)
		; Scrounger Perk
		AddToScroungerPerk(DDLL_LLS_Perk_Scrounger_Ammo_HandmadeRound, 26, 20)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")	
	elseif DLC == "ccfrsfo4002-antimaterielrifle.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccFRSFO4002_AntimaterielRifle = Game.GetFormFromFile(0x000F9C, DLC) as Weapon
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_LL_AntiMaterielRifle.AddForm(ccFRSFO4002_AntimaterielRifle, 1, 1)
		DDLL_LL_AntiMaterielRifle25.AddForm(ccFRSFO4002_AntimaterielRifle, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Gunners
		LLI_Gunner_Sniper.AddForm(DDLL_LL_AntiMaterielRifle, 30, 1)
		; Super Mutants
		LLI_Supermutant_Semiauto_Rifle_Boss.AddForm(DDLL_LL_AntiMaterielRifle, 28, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_LL_AntiMaterielRifle25, 28, 1)
		; Containers
		AddToLongWeaponLists(DDLL_LL_AntiMaterielRifle, 28, 1, false, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")	
	elseif DLC == "ccfrsfo4003-cr75l.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccFRSFO4003_LL_CR74L = Game.GetFormFromFile(0x0008AC, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccFRSFO4003_LL_CR74L.AddForm(ccFRSFO4003_LL_CR74L, 1, 1)
		DDLL_ccFRSFO4003_LL_CR74L75.AddForm(ccFRSFO4003_LL_CR74L, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Gunners
		LLI_Gunner_Weapon_High.AddForm(DDLL_ccFRSFO4003_LL_CR74L, 24, 1)
		LLI_Gunner_SemiAuto_Boss.AddForm(DDLL_ccFRSFO4003_LL_CR74L, 28, 1)
		; Railroad
		LLI_RRAgent_Weapons.AddForm(DDLL_ccFRSFO4003_LL_CR74L, 28, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccFRSFO4003_LL_CR74L75, 26, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccFRSFO4003_LL_CR74L, 28, 1, false, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")	
	elseif DLC == "ccbgsfo4042-bfg.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccBGSF04042_LL_Ammo_BFGPlasmaCell = Game.GetFormFromFile(0x000819, DLC) as LeveledItem
		ccBGSF04042_LL_Ammo_BFGPlasmaCell_Vendor = Game.GetFormFromFile(0x00081A, DLC) as LeveledItem
		ccBGSFO4042_BFGAmmoPlasmaCell = Game.GetFormFromFile(0x000804, DLC) as Ammo
		ccBGSFO4042_BFG9000 = Game.GetFormFromFile(0x000800, DLC) as Weapon
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccBGSF04042_LL_Ammo_BFGPlasmaCell.AddForm(ccBGSF04042_LL_Ammo_BFGPlasmaCell, 1, 1)
		DDLL_ccBGSF04042_LL_Ammo_BFGPlasmaCell_Vendor.AddForm(ccBGSF04042_LL_Ammo_BFGPlasmaCell_Vendor, 1, 1)
		DDLL_LLS_Perk_Scrounger_Ammo_BFGPlasmaCell.AddForm(ccBGSFO4042_BFGAmmoPlasmaCell, 1, 1)
		DDLL_ccBGSFO4042_BFG9000.AddForm(ccBGSFO4042_BFG9000, 1, 1)
		DDLL_ccBGSFO4042_BFG900025.AddForm(ccBGSFO4042_BFG9000, 1, 1)
		DDLL_ccBGSFO4042_BFG9000.AddForm(ccBGSF04042_LL_Ammo_BFGPlasmaCell, 1, 1)

		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; BoS
		LLI_BoSSoldier_MissileLauncher.AddForm(DDLL_ccBGSFO4042_BFG9000, 1, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccBGSFO4042_BFG900025, 16, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccBGSFO4042_BFG900025, 16, 1, false, true)
		; Scrounger Perk
		AddToScroungerPerk(DDLL_LLS_Perk_Scrounger_Ammo_BFGPlasmaCell, 18, 10)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccbgsfo4018-gaussrifleprototype.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccBGSFO4018_LL_Ammo_GaussRifle = Game.GetFormFromFile(0x000855, DLC) as LeveledItem
		ccBGSFO4018_LL_GaussRifle = Game.GetFormFromFile(0x000801, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccBGSFO4018_LL_Ammo_GaussRifle.AddForm(ccBGSFO4018_LL_Ammo_GaussRifle, 1, 1)
		DDLL_ccBGSFO4018_LL_GaussRifle.AddForm(ccBGSFO4018_LL_GaussRifle, 1, 1)
		DDLL_ccBGSFO4018_LL_GaussRifle50.AddForm(ccBGSFO4018_LL_GaussRifle, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Gunners
		LLI_Gunner_Sniper.AddForm(DDLL_ccBGSFO4018_LL_GaussRifle, 38, 1)
		; BoS
		LLI_BoSSoldier_SemiAuto.AddForm(DDLL_ccBGSFO4018_LL_GaussRifle, 32, 1)
		; Railroad
		LLI_RRAgent_Weapons.AddForm(DDLL_ccBGSFO4018_LL_GaussRifle, 32, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccBGSFO4018_LL_GaussRifle50, 38, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccBGSFO4018_LL_GaussRifle, 38, 1, false, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccrzrfo4001-tunnelsnakes.esm" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccRZRFO4001_LL_Pistol_Classic10mm_Simple = Game.GetFormFromFile(0x000E9A, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccRZRFO4001_LL_Pistol_Classic10mm_Simple.AddForm(ccRZRFO4001_LL_Pistol_Classic10mm_Simple, 1, 1)
		DDLL_ccRZRFO4001_LL_Pistol_Classic10mm_Simple75.AddForm(ccRZRFO4001_LL_Pistol_Classic10mm_Simple, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Raiders
		LLI_Raider_Weapons.AddForm(ccRZRFO4001_LL_Pistol_Classic10mm_Simple, 7, 1)
		; Railroad
		LLI_RRAgent_Weapons.AddForm(ccRZRFO4001_LL_Pistol_Classic10mm_Simple, 1, 1)
		; Triggermen
		LLI_Triggerman_Weapons_Pistol.AddForm(ccRZRFO4001_LL_Pistol_Classic10mm_Simple, 1, 1)
		; Minutemen
		LL_Minutemen_Weapons_NonAuto.AddForm(ccRZRFO4001_LL_Pistol_Classic10mm_Simple, 7, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccRZRFO4001_LL_Pistol_Classic10mm_Simple75, 1, 1)
		; Containers
		AddToShortWeaponLists(ccRZRFO4001_LL_Pistol_Classic10mm_Simple, 1, 1, true, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccswkfo4001-astronautpowerarmor.esm" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccSWKFO4001_CC1EnergyGun = Game.GetFormFromFile(0x000864, DLC) as Weapon
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccSWKFO4001_CC1EnergyGun.AddForm(ccSWKFO4001_CC1EnergyGun, 1, 1)
		DDLL_ccSWKFO4001_CC1EnergyGun25.AddForm(ccSWKFO4001_CC1EnergyGun, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Institute
		LLI_Synth_Weapons.AddForm(DDLL_ccSWKFO4001_CC1EnergyGun, 1, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccSWKFO4001_CC1EnergyGun25, 21, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccSWKFO4001_CC1EnergyGun, 21, 1, false, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccrzrfo4002-disintegrate.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccRZRFO4002_LL_Atomizer = Game.GetFormFromFile(0x000881, DLC) as LeveledItem
		ccRZRFO4002_LL_Disintegrator = Game.GetFormFromFile(0x0008AD, DLC) as LeveledItem
		ccRZRFO4002_AlienShockBaton = Game.GetFormFromFile(0x00084F, DLC) as Weapon
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccRZRFO4002_LL_Atomizer.AddForm(ccRZRFO4002_LL_Atomizer, 1, 1)
		DDLL_ccRZRFO4002_LL_Disintegrator.AddForm(ccRZRFO4002_LL_Disintegrator, 1, 1)
		DDLL_ccRZRFO4002_AlienShockBaton.AddForm(ccRZRFO4002_AlienShockBaton, 1, 1)
		DDLL_ccRZRFO4002_LL_Atomizer25.AddForm(ccRZRFO4002_LL_Atomizer, 1, 1)
		DDLL_ccRZRFO4002_LL_Disintegrator25.AddForm(ccRZRFO4002_LL_Disintegrator, 1, 1)
		DDLL_ccRZRFO4002_AlienShockBaton25.AddForm(ccRZRFO4002_AlienShockBaton, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Institute
		LLI_Synth_Weapons.AddForm(DDLL_ccRZRFO4002_LL_Disintegrator, 1, 1)
		LLI_Synth_Weapons.AddForm(DDLL_ccRZRFO4002_LL_Atomizer, 1, 1)
		LLI_Hostile_Synth_Melee.AddForm(DDLL_ccRZRFO4002_AlienShockBaton, 1, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccRZRFO4002_LL_Disintegrator25, 17, 1)
		AddToVendorWeaponLists(DDLL_ccRZRFO4002_LL_Atomizer25, 17, 1)
		AddToVendorWeaponLists(DDLL_ccRZRFO4002_AlienShockBaton25, 17, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccRZRFO4002_LL_Disintegrator, 21, 1, false, true)
		AddToShortWeaponLists(DDLL_ccRZRFO4002_LL_Atomizer, 21, 1, false, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccbgsfo4116-heavyflamer.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccBGSFO4116_LL_HeavyFlamer = Game.GetFormFromFile(0x00082B, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccBGSFO4116_LL_HeavyFlamer.AddForm(ccBGSFO4116_LL_HeavyFlamer, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		LL_Flamer.AddForm(DDLL_ccBGSFO4116_LL_HeavyFlamer, 1, 1)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccsbjfo4002_manwellrifle.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccSBJFO4002_LL_ManwellRifle = Game.GetFormFromFile(0x00091D, DLC) as LeveledItem
		ccSBJFO4002_LL_ManwellRifleNonCarbine = Game.GetFormFromFile(0x00099A, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccSBJFO4002_LL_ManwellRifle.AddForm(ccSBJFO4002_LL_ManwellRifle, 1, 1)
		DDLL_ccSBJFO4002_LL_ManwellRifle25.AddForm(ccSBJFO4002_LL_ManwellRifle, 1, 1)
		DDLL_ccSBJFO4002_LL_ManwellRifleNonCarbine.AddForm(ccSBJFO4002_LL_ManwellRifleNonCarbine, 1, 1)
		DDLL_ccSBJFO4002_LL_ManwellRifleNonCarbine25.AddForm(ccSBJFO4002_LL_ManwellRifleNonCarbine, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Gunners
		LLI_Gunner_Sniper.AddForm(DDLL_ccSBJFO4002_LL_ManwellRifle, 35, 1)
		; Railroad
		LLI_RRAgent_Weapons.AddForm(DDLL_ccSBJFO4002_LL_ManwellRifleNonCarbine, 28, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccSBJFO4002_LL_ManwellRifle25, 38)
		AddToVendorWeaponLists(DDLL_ccSBJFO4002_LL_ManwellRifleNonCarbine25, 35, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccSBJFO4002_LL_ManwellRifle, 32, 1, false, true)
		AddToLongWeaponLists(DDLL_ccSBJFO4002_LL_ManwellRifleNonCarbine, 30, 1, false, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccbgsfo4047-qthund.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccBGSFO4047_DefaultThunderbolt = Game.GetFormFromFile(0x000872, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccBGSFO4047_DefaultThunderbolt.AddForm(ccBGSFO4047_DefaultThunderbolt, 1, 1)
		DDLL_ccBGSFO4047_DefaultThunderbolt25.AddForm(ccBGSFO4047_DefaultThunderbolt, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Institute
		LLI_Synth_Weapons.AddForm(DDLL_ccBGSFO4047_DefaultThunderbolt, 1, 1)
		; BoS
		LLI_BoSSoldier_SemiAuto.AddForm(DDLL_ccBGSFO4047_DefaultThunderbolt, 1, 1)
		; Gunners
		LLI_Gunner_SemiAuto_Boss.AddForm(DDLL_ccBGSFO4047_DefaultThunderbolt, 15, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccBGSFO4047_DefaultThunderbolt25, 15, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccBGSFO4047_DefaultThunderbolt, 15, 1)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccbgsfo4046-tescan.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccBGSFO4046_LL_TesCanWeapon = Game.GetFormFromFile(0x00001E, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccBGSFO4046_LL_TesCanWeapon.AddForm(ccBGSFO4046_LL_TesCanWeapon, 1, 1)
		DDLL_ccBGSFO4046_LL_TesCanWeapon25.AddForm(ccBGSFO4046_LL_TesCanWeapon, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; BoS
		LLI_BoSSoldier_MissileLauncher.AddForm(DDLL_ccBGSFO4046_LL_TesCanWeapon, 1, 1)
		; Super Mutants
		LLI_Supermutant_Missile.AddForm(DDLL_ccBGSFO4046_LL_TesCanWeapon, 1, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccBGSFO4046_LL_TesCanWeapon25, 20, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccBGSFO4046_LL_TesCanWeapon, 20, 1, false, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccbgsfo4048-dovah.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccBGSFO4048_Sword = Game.GetFormFromFile(0x000F9B, DLC) as Weapon
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccBGSFO4048_Sword.AddForm(ccBGSFO4048_Sword, 1, 1)
		DDLL_ccBGSFO4048_Sword25.AddForm(ccBGSFO4048_Sword, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Raiders
		LLI_Raider_Melee_Standard.AddForm(DDLL_ccBGSFO4048_Sword, 24, 1)
		; Gunners
		LLI_Gunner_Melee.AddForm(DDLL_ccBGSFO4048_Sword, 22, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccBGSFO4048_Sword25, 22, 1)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccsbjfo4001-solarflare.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccSBJFO4001_LL_SolarFlare_Rifle_Standard = Game.GetFormFromFile(0x0008D5, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccSBJFO4001_LL_SolarFlare_Rifle_Standard.AddForm(ccSBJFO4001_LL_SolarFlare_Rifle_Standard, 1, 1)
		DDLL_ccSBJFO4001_LL_SolarFlare_Rifle_Standard25.AddForm(ccSBJFO4001_LL_SolarFlare_Rifle_Standard, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Gunners
		LLI_Gunner_SemiAuto_Boss.AddForm(DDLL_ccSBJFO4001_LL_SolarFlare_Rifle_Standard, 28, 1)
		; BoS
		LLI_BoSSoldier_SemiAuto.AddForm(DDLL_ccSBJFO4001_LL_SolarFlare_Rifle_Standard, 1, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccSBJFO4001_LL_SolarFlare_Rifle_Standard25, 28, 1)
		; Containers
		AddToLongWeaponLists(DDLL_ccSBJFO4001_LL_SolarFlare_Rifle_Standard, 28, 1, false, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccbgsfo4035-pint.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccBGSFO4035_PintSizedSlasherKnife = Game.GetFormFromFile(0x000809, DLC) as Weapon
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccBGSFO4035_PintSizedSlasherKnife.AddForm(ccBGSFO4035_PintSizedSlasherKnife, 1, 1)
		DDLL_ccBGSFO4035_PintSizedSlasherKnife25.AddForm(ccBGSFO4035_PintSizedSlasherKnife, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Raiders
		LLI_Raider_Melee_Simple.AddForm(ccBGSFO4035_PintSizedSlasherKnife, 12, 1)
		; Gunners
		LLI_Gunner_Melee.AddForm(ccBGSFO4035_PintSizedSlasherKnife, 12, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccBGSFO4035_PintSizedSlasherKnife25, 12, 1)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccbgsfo4117-capmerc.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccBGSFO4117_CMArmorRandom = Game.GetFormFromFile(0x000039, DLC) as LeveledItem
		ccBGSFO4117_CMArmorNoDress = Game.GetFormFromFile(0x000770, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccBGSFO4117_CMArmorRandom.AddForm(ccBGSFO4117_CMArmorRandom, 1, 1)
		DDLL_ccBGSFO4117_CMArmorNoDress.AddForm(ccBGSFO4117_CMArmorNoDress, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Raiders
		LLI_Armor_Raider.AddForm(DDLL_ccBGSFO4117_CMArmorRandom, 1, 1)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "ccfsvfo4001-modularmilitarybackpack.esl" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		FSVFO4001_LVLI_BackpackBase = Game.GetFormFromFile(0x000919, DLC) as LeveledItem
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_FSVFO4001_LVLI_BackpackBase.AddForm(FSVFO4001_LVLI_BackpackBase, 1, 1)
		DDLL_FSVFO4001_LVLI_BackpackBase25.AddForm(FSVFO4001_LVLI_BackpackBase, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Raiders
		LLI_Armor_Raider01.AddForm(DDLL_FSVFO4001_LVLI_BackpackBase25, 1, 1)
		LLI_Armor_Raider02b_3.AddForm(DDLL_FSVFO4001_LVLI_BackpackBase25, 1, 1)
		LLI_Armor_Raider_Regular_New.AddForm(DDLL_FSVFO4001_LVLI_BackpackBase25, 1, 1)
		; Gunners
		LL_Armor_Gunner01.AddForm(DDLL_FSVFO4001_LVLI_BackpackBase25, 1, 1)
		LLI_Armor_Gunner_Outfit.AddForm(DDLL_FSVFO4001_LVLI_BackpackBase25, 1, 1)
		; BoS
		LLI_Armor_BoSCombatArmor_Outfit.AddForm(DDLL_FSVFO4001_LVLI_BackpackBase25, 1, 1)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "cczsefo4002-smanor.esm" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.
		ccZSEFO4002_Deliverer = Game.GetFormFromFile(0x001202, DLC) as Weapon
		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.
		DDLL_ccZSEFO4002_Deliverer.AddForm(ccZSEFO4002_Deliverer, 1, 1)
		DDLL_ccZSEFO4002_Deliverer25.AddForm(ccZSEFO4002_Deliverer, 1, 1)
		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.
		; Raiders
		LLI_Raider_Weapons.AddForm(DDLL_ccZSEFO4002_Deliverer, 7, 1)
		; Railroad
		LLI_RRAgent_Weapons.AddForm(DDLL_ccZSEFO4002_Deliverer, 1, 1)
		; Triggermen
		LLI_Triggerman_Weapons_Pistol.AddForm(DDLL_ccZSEFO4002_Deliverer, 1, 1)
		; Minutemen
		LL_Minutemen_Weapons_NonAuto.AddForm(DDLL_ccZSEFO4002_Deliverer, 7, 1)
		; Vendors
		AddToVendorWeaponLists(DDLL_ccZSEFO4002_Deliverer25, 1, 1)
		; Containers
		AddToShortWeaponLists(DDLL_ccZSEFO4002_Deliverer, 1, 1, true, true)

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	elseif DLC == "TEMPLATE" && installedVar.getValue() == 1 && distributedVar.getValue() == 0
		; Get the Leveled Lists.

		debug.trace( self + "DDLL finished grabbing " + DLC + " Leveled Lists.")

		; Add the Leveled Lists to the DDLL Leveled Lists for better control.

		debug.trace( self + "DDLL finished adding " + DLC + " lists to DDLL lists.")

		; Add the DDLL Leveled Lists to the game.

		distributedVar.setValue(1)
		debug.trace( self + "DDLL finished distributing DDLL " + DLC + " lists.")
	else
		if ErrorCode == 1
			debug.trace( self + "WARNING: DLC with plugin name " + DLC + " was ignored because its quest requirements weren't met.")
		elseif ErrorCode == 2
			debug.trace( self + "WARNING: DLC with plugin name " + DLC + " was ignored because its above the user-defined distribution threshold of " + DDLL_OutOfUniverseDistributionAllowed.GetValue() + ". Item threshold is: " + inUniverseTier)
		Elseif ErrorCode == 3
			debug.trace( self + "WARNING: DLC with plugin name " + DLC + " is not installed.")
		else
			if installedVar.getValue() == 1 && distributedVar.getValue() == 1
				debug.trace( self + "WARNING: DLC with plugin name " + DLC + " was ignored because it was already distributed.")
			else
				debug.trace( self + "WARNING: DLC with plugin name " + DLC + " was ignored, either because it was already distributed, or because it was not found.")
			endif
		endif
	endif
EndFunction

Function AddToVendorArmorLists(LeveledItem itemToAdd, int Level = 1, int Quantity = 1, Bool addToBoS = true)
	;Shared Lists
	;Lucas Miller, Fallon's Basement, Rufus, Penny, Daisy, Trashcan Carla, DC Surplus, Atom Cats, Drumlin Diner, Opal, Tinker Tom
	
	VL_Vendor_Armor.AddForm(itemToAdd as Form, Level, Quantity)
	VL_Vendor_Clothing.AddForm(itemToAdd as Form, Level, Quantity)
	VL_Vendor_General.AddForm(itemToAdd as Form, Level, Quantity)
	VL_Vendor_General_Drumlin.AddForm(itemToAdd as Form, Level, Quantity)
	VL_Vendor_Weapon.AddForm(itemToAdd as Form, Level, Quantity)
	
	LLC_DCShopArmor.AddForm(itemToAdd as Form, Level, Quantity)

	;Proctor Teagan
	if(AddToBoS)
		LLI_Armor_BoSCombatArmor_Vendor_Outfit.AddForm(itemToAdd as Form, Level, Quantity)
	endIf
EndFunction

Function AddToVendorWeaponLists(LeveledItem itemToAdd, int Level = 1, int Quantity = 1, Bool addToBoS = true)
	;Shared Lists
	;Arturo/Diamond City Weapons, Kill or Be Killed, Cricket, Tinker Tom, Eleanor, Workshop Vendor level 3 and 4
	
	VL_Vendor_Weapon.AddForm(itemToAdd as Form, Level, Quantity)		
	LL_Vendor_Weapon_GunSpecialty.AddForm(itemToAdd as Form, Level, Quantity)
	
	;Proctor Teagan
	if(AddToBoS)
		LL_Vendor_Weapon_GunBoS.AddForm(itemToAdd as Form, Level, Quantity)
	endIf	
EndFunction

Function AddToAmmoLists(LeveledItem itemToAdd, int Level = 1, int Quantity = 1, String List = "Normal")
	if List == "Normal"
		LL_Ammo_Any.AddForm(itemToAdd, Level, Quantity)
		LL_Ammo_Any_Same.AddForm(itemToAdd, Level, Quantity)
	elseif List == "Rare"
		LLI_Loot_Ammo_Rare.AddForm(itemToAdd, Level, Quantity)
	elseif List == "Unique"
		LLI_Loot_Ammo_Unique.AddForm(itemToAdd, Level, Quantity)
	endif
EndFunction

Function AddToLongWeaponLists(LeveledItem itemToAdd, int Level = 1, int Quantity = 1, Bool addToRaiders = false, Bool addToBoss = false)
	LL_Weapons_Guns_Long.AddForm(itemToAdd, Level, Quantity)
	if addToRaiders == true
		LLC_Weapons_Guns_Raider_Long.AddForm(itemToAdd, Level, Quantity)
	endif
	if addToBoss == true
		LL_Weapon_Any_Boss.AddForm(itemToAdd, Level, Quantity)
	endif
EndFunction

Function AddToShortWeaponLists(LeveledItem itemToAdd, int Level = 1, int Quantity = 1, Bool addToRaiders = false, Bool addToBoss = false)
	LL_Weapons_Guns_Short.AddForm(itemToAdd, Level, Quantity)
	if addToRaiders == true
		LLC_Weapons_Guns_Raider_Short.AddForm(itemToAdd, Level, Quantity)
	endif
	if addToBoss == true
		LL_Weapon_Any_Boss.AddForm(itemToAdd, Level, Quantity)
	endif
EndFunction

Function AddToScroungerPerk(LeveledItem itemToAdd, int Level = 1, int Quantity = 1, bool notSmall = false)
	if notSmall == true
		LLE_Perk_Scrounger100.AddForm(itemToAdd, Level, Quantity)
	else
		LLE_Perk_Scrounger100.AddForm(itemToAdd, Level, Quantity)
		LLE_Perk_ScroungerSmall.AddForm(itemToAdd, Level, Quantity)
	endif
EndFunction
