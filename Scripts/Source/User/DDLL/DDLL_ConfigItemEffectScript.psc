Scriptname DDLL:DDLL_ConfigItemEffectScript extends activemagiceffect
{Shows the Configuration Message when consumed.}

DDLL_MasterScript Property DDLL_Master Auto Const Mandatory

Event OnEffectStart(Actor akTarget, Actor akCaster)
	DDLL_Master.DDLLConfigMessage()
EndEvent